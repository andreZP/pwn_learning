Dr.rer.oec.Gadget IDAPython Plugin
==================================

This is an IDAPython plugin for the
Interactive Disassembler for all your
ROP experimentation needs ;-)

Develop and analyze ROP exploits for
all processor architectures supported
by IDA itself!

what's new:
===========

* This is a fork of the 'original' Dr.Gadget plugin
* State is saved to and loaded from IDB by default
* (hopefully universal) support for all processor modules
  supported by IDA itself (including 64bit etc.)
* "plugin" interface - write a plugin for this plugin! ;-)
* IDA disassembly is kept in its original state
* hotkeys "1" and "2" for decrementing and incrementing
  an item's value, respectively (yay, more interactivity).
  this allows you to interactively "browse" the disassembly
  and memory for useful gadgets
* context-sensitive content viewers
  - now used to display data by default
  - visually highlight return instructions
* show extended information about modules (ASLR/DEP)

how to install:
===============

* copy the following files and folders to your IDA/plugins directory:
  - drgadget.py
  - drgadget

how to use:
===========

* restart IDA
* use the alt-F5 hotkey to invoke the plugin or
  invoke the plugin from the IDA plugin menu
* right-click the Dr.Gadget window for a popup
  menu to appear.
* use the context-sensitive popup menu to add/modify/delete
  items and to invoke Dr.Gadget plugins, such as the instruction finder
* with an item selected, use "1" and "2" on your keyboard to decrement
  and increment the item's value, respectively
* the plugin may be used both during an active debugging
  session (recommended!) and on a static disassembly listing
