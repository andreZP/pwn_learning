BITS 64


;sys_socket
    mov rdx, 0  ;protocol
    mov rsi, 1  ;type = SOCK_STREAM
    mov rdi, 2  ;domain = AF_INET
    mov rax, 41 ;socket
    syscall

;sys_connect
    mov rdx, 16
    ;push 0x0100007f ; sin_addr
    ;push 0x3905    ; sin_port
    ;mov rdi, 0x000239050100007f         ;family = AF_INET
    mov rdi, 0x0100007f39050002         ;family = AF_INET
    push rdi
    mov rsi, rsp   ; sockaddr
    mov rdi, rax  ; socket
    mov rax, 42    ; connect
    syscall

;sys_exit
    mov rdi, 0
    mov rax, 60
    syscall




