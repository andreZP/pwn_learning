
struct dnsHdr {
    unsigned short id;
    unsigned short flags;
    unsigned short questions;
    unsigned short answers;
    unsigned short authority_rrs;
    unsigned short additional_rrs;
};

