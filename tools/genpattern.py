import string
import sys

def pattern_create(size=20280,offset=0):
	# Aa0Aa1Aa2Aa3Aa4Aa5Aa6Aa7Aa8Aa9Ab0Ab1Ab2Ab3Ab4Ab5Ab6Ab7Ab8Ab9Ac0A [...]
	return "".join("".join("".join(u+l+d for d in string.digits) for l in string.lowercase) for u in string.uppercase)[offset:offset+size]

def pattern_offset(str, offset=0):
	o = pattern_create(offset=offset).find(str)
	if o >= 0:
		return o
	else: # try reversed string
		return pattern_create().find(str[::-1])

def main():
	if len(sys.argv) < 2:
		print "[+] Usage: %s [-g] <length> [-c] <string 4 calc>" % sys.argv[0]
		return
	if sys.argv[1] == '-g':
		print pattern_create(int(sys.argv[2]))
	if sys.argv[1] == '-c':
		s = sys.argv[2] 
		if sys.argv[2].startswith('0x'):
			s = sys.argv[2][2:].decode('hex')[::-1]
		print pattern_offset(s)


if __name__ == '__main__':
	main()