#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdarg.h>
#include <syslog.h>

/* gcc -Wall -m32 ch3.c -o ch3 -z execstack */

extern char **environ;
void respond(char *fmt,...);

int vul(void)
{
  char tmp[1024];
  char buf[1024];
  int len = 0;

  while(1) {

    memset(buf, 0, sizeof(buf));
    memset(tmp, 0, sizeof(tmp));

    if ( (len = read(0, buf, sizeof(buf))) <= 0 ) {
      exit(-1);
    } 

    if (!strncmp(buf, "quit", 4)) {
      respond("bye bye ...\n");
      return 0;
    }
    snprintf(tmp, sizeof(tmp)-1, buf);
    respond("%s", tmp);

  }
}

void respond(char *fmt,...)
{
  va_list va;
  char buf[1024];

  va_start(va,fmt);
  vsnprintf(buf,sizeof(buf),fmt,va);
  va_end(va);
  write(STDOUT_FILENO,buf,strlen(buf));
}


int main()
{
  int i,len = sizeof(struct sockaddr_in);
  char login[16];
  char passwd[1024];

  for(i=0; environ[i] != NULL; i++) {
     memset(environ[i], '\0', strlen(environ[i]));
  }

  /* get login */
  memset(login, 0, sizeof(login));
  respond("login: ");
  if ( (len = read(0, login, sizeof(login))) <= 0 ) {
    exit(-1);
  } 

  /* get passwd */
  memset(passwd, 0, sizeof(passwd));
  respond("password: ");
  if ( (len = read(0, passwd, sizeof(passwd))) <= 0 ) {
    exit(-1);
  } 

  write(STDOUT_FILENO,"Welcome to our network service! Type \"quit\" to return.\n",55);
  /* let's run ... */
  vul();
  return 0;
}

