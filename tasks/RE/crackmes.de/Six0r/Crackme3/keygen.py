#!/usr/bin/python
import sys

def transform_name(st):
    ret = 0x7E4C9E32
    for i in st:
        ret =  (ret * ord(i)) & 0xffffffff
    return ret

def transform1(numb, const2, const1):
    ret = 1
    while const2 > 0:
        if const2 & 1 == 1:
            ret = ((ret * numb )) % const1
        const2 >>= 1
        numb = ((numb * numb)) % const1
    return ret
    
def transform2(serial2, multiply_res2, mag_const):
    a1 = serial2
    a2 = multiply_res2
    a1 *= a2
    return a1 % mag_const

def apply_transforms(name, keyl, keyr):
    m_name = transform_name(name)
    m_key_r = transform1(keyr, 0xF2A5, 0xF2A7)
    m_key_r_name = transform2(m_key_r, m_name, 0xF2A7)
    m_key_r_name_key_l = transform2(keyl, m_key_r, 0xF2A7)

    ret = transform1(0x15346, m_key_r_name, 0x3CA9D)
    ret2 = transform1(0x307c7, m_key_r_name_key_l, 0x3CA9D)
    return transform2(ret2, ret, 0x3ca9d) % 0xf2a7

def main():
    name = raw_input('provide name 6 byte: ')
    if len(name) != 6:
        print 'fuck'
        sys.exit(0)

    for i in xrange(0, 65535):
        kl = 0x1234
        kr = i
        ret = apply_transforms(name, kl, kr)
        if ret == kl:
            print '%s-%s' % (hex(kl)[2:].rjust(4, '0'), hex(kr)[2:].rjust(4, '0'))
    sys.exit(0)

main()
