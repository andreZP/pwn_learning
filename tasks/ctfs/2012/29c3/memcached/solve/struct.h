struct dataEntry{
	unsigned short has_fd;
	unsigned short keyLen;
	int            valueLen;
	char           key[0x80];
	char           value[0x800];
	void* fd;
	void* next;
};
