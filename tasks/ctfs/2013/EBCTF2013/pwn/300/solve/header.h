struct SlurpFile {
    char* fname;
    char* content;
    int size;
};

struct HashListEntry {
    char* fname;
    int isDir;
    char* content;
};

struct Hashlist {
struct HashListEntry* entries;
int numberOfElements;
};

