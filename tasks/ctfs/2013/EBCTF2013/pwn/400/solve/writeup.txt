file dimwit
dimwit: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked (uses shared libs), for GNU/Linux 2.6.24, not stripped

1) Sets up tcp server on port 50001
2) On new connect it creates udp socket, tries to bind it on 53 port, and in fail case on random port until success bind.
3) Then it sends dns request to server from command line, and tries to resolve name from command line too. 
If there`s no any packet during 5 seconds timeout programm calls exit().
4) Received dns packet is checked by flags(0x8180) and id field. Id is unknown for us, so we have to brute it and send 65535 packets.
After these checks dimwit validates questions buffer in dns (it should not be more then whole packet len). In my exploit questions conut field  is set to 0.
5) After questions validation dimwit calls vulnerable copy_from_labelname() functions. It is responsible for copy name in view "\x03www\x02ya\x02ru\x00" to "www.ya.ru".
In order to exploit buffer overflow we have to understand next things: every part of dns name can't be more than 63 (\x03 in \x03www),
but when copy_from_labelname() meets byte more then 0xbf, it sets next byte as new offset, in all other cases abort() call will be. 
So we can construct buffer like: 
[62, 59, 56, 63, etc. 60(in order to overwrite rest of this buf and set cursor to start of the next buf), \xc0, offset_of_this_buf, \xc0, offset_of_this_buf + 1, offset_of_this_buf+2, etc] => [62, 59, 56, 53, etc ...] => [62, 59, 56, 53, ..., 60 ] => [payload size][final payload] to copy arbitrary count of bytes. 
Because there is no check on destination buffer size, we should write 568 bytes and overwrite receive_dns() return address. 
6) Rop chain: pop_rdi instruction address + "flag" variable address + read_motd() address. read_motd() will read flag file to our socket.
