
struct MsgEntry {
    char         repliesCnt;
    struct MsgEntry*    next;
    struct MsgEntry*    prev;
    void*        UpdateCb;
    void*        FreeRepliesCb;
    void*        repliesList;
    int          id;
    char*        author;
    char*        title;
    int          rndVal;
    int          state;
};

struct ReplyEntry {
    int   state;
    int   msgID;
    char* banner;
    void* replyStateModifier;
    void* replyFreer;
    void* next;
};
