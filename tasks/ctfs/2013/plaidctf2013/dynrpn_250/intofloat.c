#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char** argv) 
{
	union {
		double dval;
		volatile unsigned long lval;
	} foo;

	unsigned long target;

	if (argc < 2 || !(target = strtoul(argv[1], NULL, 16)) || target >= (1ul << 48)) {
		printf("error\n");
		return 1;
	}

	// double value is stored in memory:
	//0           01111111111              0000000000000000000000000000000000000000000000000000
	//sign bit    exponent(11bit): 1023    precision(52 bit): 0
	// and calculated: ±sign * (1 + (exponent/2^52) ) * ((2^exponent) − 1023)

	// OR target with minimal exponent and zero precision to get it val in double form
	foo.lval = target | (1023ul << 52);

	int i,j;

	for (i = 0; i < 256; i++) {
		volatile double x = (unsigned long) foo.dval;
		// if least 4 bytes of double are equal to target
		if (target == (*((unsigned long*) &x) & 0xffffffffffff)) {
			// split it to two multipliers + residue
			double s = sqrt(x);
			unsigned long tmp = s;
			unsigned long tmp2 = ((unsigned long) x) - (tmp * tmp);
			printf("%lu %lu * %lu + \n", tmp, tmp, tmp2);
			break;
		}
		// increase exponent value
		foo.lval += 1ul << 52;
	}
	return 0;
}
