#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
 
int main(int argc, char** argv) {
 
    union {
        double dval;
        volatile unsigned long lval;
    } foo;
 
    unsigned long target;
 
    if (argc < 2 || !(target = strtoul(argv[1], NULL, 16)) || target >= (1ul << 48)) {
        printf("error\n");
        return 1;
    }
 
    foo.lval = target | (1023ul << 52);
     
    int i,j;
 
    for (i = 0; i < 256; i++) {
        volatile double x = (unsigned long) foo.dval;
        if (target == (*((unsigned long*) &x) & 0xffffffffffff)) {
            //printf("%ld ", (long) foo.dval);
            double s = sqrt(x);
            unsigned long tmp = s;
            unsigned long tmp2 = ((unsigned long) x) - (tmp * tmp);
            printf ("%lu %lu * %lu + \n", tmp, tmp, tmp2);
        }
        //printf("%d %016lx %lu\n", i, *((unsigned long*) &x), (unsigned long) foo.dval);
        foo.lval += 1ul << 52;
    }
 
    return 0;
}