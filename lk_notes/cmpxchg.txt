cmpxchg_futex_value_locked 

old val usually 0
new val usually pid
compares *uaddr with oldval, if it equals, newval saved in uaddr, oldval always returned in uval to detect deadlock (if pid in uaddr is the same as in new val)
static inline int futex_atomic_cmpxchg_inatomic(u32 *uval, u32 __user *uaddr, u32 oldval, u32 newval)
     int ret = 0;
     asm volatile("1:\t" LOCK_PREFIX "cmpxchgl %4, %2\n"
                  "2:\t.section .fixup, \"ax\"\n"
                  "3:\tmov     %3, %0\n"
                  "\tjmp     2b\n"
                  "\t.previous\n"
                  _ASM_EXTABLE(1b, 3b)
                  : "+r" (ret), "=a" (oldval), "+m" (*uaddr)
                  : "i" (-EFAULT), "r" (newval), "1" (oldval)
                  : "memory"
     );
     *uval = oldval;

     +r     - + means operand both read/written, r mean in any GPR;
     =a     - = means register is write changed, a mean eax;
     +m     - m mean memory operand
     i      - integer constant
     r      - use any GPR
     1      - store it in first argument, in oldval
     memory - tell gcc that content in memory will be changed
     +---+--------------------+
     | r |    Register(s)     |
     +---+--------------------+
     | a |   %eax, %ax, %al   |
     | b |   %ebx, %bx, %bl   |
     | c |   %ecx, %cx, %cl   |
     | d |   %edx, %dx, %dl   |
     | S |   %esi, %si        |
     | D |   %edi, %di        |
     +---+--------------------+


